#include "http_server_app.h"
/* Simple HTTP Server Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include <esp_wifi.h>
#include <esp_event.h>
#include <esp_log.h>
#include <esp_system.h>

#include <sys/param.h>

#include "esp_netif.h"
#include "esp_eth.h"
#include "output_iot.h"
#include <string.h>
#include <esp_http_server.h>

/* A simple example that demonstrates how to create GET and POST
 * handlers for the web server.
 */

static const char *TAG = "example";
static httpd_handle_t server = NULL;
extern const uint8_t index_html_start[] asm("_binary_index_html_start");
extern const uint8_t index_html_end[] asm("_binary_index_html_end");
static  http_post_callback_t http_switch_callback = NULL;
static http_get_callback_t http_dht11_callback = NULL;
/* An HTTP GET handler */
static esp_err_t hello_get_handler(httpd_req_t *req)
{
 
    httpd_resp_set_type(req,"text/html");
    httpd_resp_send(req, (const char *)index_html_start, index_html_end - index_html_start);
    return ESP_OK;
    /* After sending the HTTP response the old HTTP request
     * headers are lost. Check if HTTP request headers can be read now. */
    
}

static const httpd_uri_t get_dht11 = {
    .uri       = "/dht11",
    .method    = HTTP_GET,
    .handler   = hello_get_handler,
    /* Let's pass response string in user
     * context to demonstrate it's usage */
    .user_ctx  = NULL
};

//
static esp_err_t hello_get1_handler(httpd_req_t *req)
{
 
     const char* resp_str = (const char*) "{\"temperature\": \"27.3\",\"huminity\": \"80\"}";
     httpd_resp_send(req, resp_str, strlen(resp_str));
    http_dht11_callback();
    return ESP_OK;
    /* After sending the HTTP response the old HTTP request
     * headers are lost. Check if HTTP request headers can be read now. */
    
}

static const httpd_uri_t get_data_dht11 = {
    .uri       = "/getdatadht11",
    .method    = HTTP_GET,
    .handler   = hello_get1_handler,
    /* Let's pass response string in user
     * context to demonstrate it's usage */
    .user_ctx  = NULL
};
/* An HTTP POST handler */
static esp_err_t echo_post1_handler(httpd_req_t *req)
{

    char buf[100];
 
    httpd_req_recv(req, buf, req->content_len);
     //printf("data:%s\n",buf); 
    http_switch_callback(buf,req ->content_len);              
    httpd_resp_send_chunk(req, NULL, 0);
    return ESP_OK;
}

static const httpd_uri_t post_switch1_data = {
    .uri       = "/switch1",
    .method    = HTTP_POST,
    .handler   = echo_post1_handler,
    .user_ctx  = NULL
};
//
static esp_err_t echo_post_handler(httpd_req_t *req)
{

    //
    char buf[100];

    httpd_req_recv(req, buf, req->content_len);
    // printf("data:%s\n",buf);                  
    httpd_resp_send_chunk(req, NULL, 0);
    return ESP_OK;
}

static const httpd_uri_t post_data = {
    .uri       = "/data",
    .method    = HTTP_POST,
    .handler   = echo_post_handler,
    .user_ctx  = NULL
};
/* This handler allows the custom error handling functionality to be
 * tested from client side. For that, when a PUT request 0 is sent to
 * URI /ctrl, the /hello and /echo URIs are unregistered and following
 * custom error handler http_404_error_handler() is registered.
 * Afterwards, when /hello or /echo is requested, this custom error
 * handler is invoked which, after sending an error message to client,
 * either closes the underlying socket (when requested URI is /echo)
 * or keeps it open (when requested URI is /hello). This allows the
 * client to infer if the custom error handler is functioning as expected
 * by observing the socket state.
 */
esp_err_t http_404_error_handler(httpd_req_t *req, httpd_err_code_t err)
{
    if (strcmp("/dht11", req->uri) == 0) {
        httpd_resp_send_err(req, HTTPD_404_NOT_FOUND, "/hello URI is not available");
        /* Return ESP_OK to keep underlying socket open */
        return ESP_OK;
    }   
    /* For any other URI send 404 and close socket */
    httpd_resp_send_err(req, HTTPD_404_NOT_FOUND, "Some 404 error message");
    return ESP_FAIL;
}

/* An HTTP PUT handler. This demonstrates realtime
 * registration and deregistration of URI handlers
 */
// static esp_err_t ctrl_put_handler(httpd_req_t *req)
// {
//     char buf;
//     int ret;

//     if ((ret = httpd_req_recv(req, &buf, 1)) <= 0) {
//         if (ret == HTTPD_SOCK_ERR_TIMEOUT) {
//             httpd_resp_send_408(req);
//         }
//         return ESP_FAIL;
//     }

//     if (buf == '0') {
//         /* URI handlers can be unregistered using the uri string */
//         ESP_LOGI(TAG, "Unregistering /hello and /echo URIs");
//         httpd_unregister_uri(req->handle, "/hello");
//         httpd_unregister_uri(req->handle, "/echo");
//         /* Register the custom error handler */
//         httpd_register_err_handler(req->handle, HTTPD_404_NOT_FOUND, http_404_error_handler);
//     }
//     else {
//         ESP_LOGI(TAG, "Registering /hello and /echo URIs");
//         httpd_register_uri_handler(req->handle, &hello);
//         httpd_register_uri_handler(req->handle, &echo);
//         /* Unregister custom error handler */
//         httpd_register_err_handler(req->handle, HTTPD_404_NOT_FOUND, NULL);
//     }

//     /* Respond with empty body */
//     httpd_resp_send(req, NULL, 0);
//     return ESP_OK;
// }

// static const httpd_uri_t ctrl = {
//     .uri       = "/ctrl",
//     .method    = HTTP_PUT,
//     .handler   = ctrl_put_handler,
//     .user_ctx  = NULL
// };

 void start_webserver(void)
{
 
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    config.lru_purge_enable = true;

    // Start the httpd server
    ESP_LOGI(TAG, "Starting server on port: '%d'", config.server_port);
    if (httpd_start(&server, &config) == ESP_OK) {
        // Set URI handlers
        ESP_LOGI(TAG, "Registering URI handlers");
        httpd_register_uri_handler(server,&get_dht11);
        httpd_register_uri_handler(server, &get_data_dht11);
        httpd_register_uri_handler(server, &post_data);
        httpd_register_uri_handler(server, &post_switch1_data);
        httpd_register_err_handler(server, HTTPD_404_NOT_FOUND, http_404_error_handler);
        //httpd_register_uri_handler(server, &post_data);
        //httpd_register_uri_handler(server, &ctrl);
     
    }

    ESP_LOGI(TAG, "Error starting server!");
    return NULL;
}

 void stop_webserver(void)
{
    // Stop the httpd server
    httpd_stop(server);
}

void http_set_dht11_callback(void*cb)
{
    http_dht11_callback = cb;
}
void http_set_switch_callback(void*cb)
{
    http_switch_callback=cb;
}


