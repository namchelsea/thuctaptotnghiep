#ifndef _HTTP_SERVER_APP_H
#define _HTTP_SERVER_APP_H
typedef void (*http_post_callback_t)(char*data,int len);
typedef void (*http_get_callback_t)(void);
 void start_webserver(void);
 void stop_webserver(void);
 void http_set_dht11_callback(void*cb);
 void http_set_switch_callback(void*cb);
#endif